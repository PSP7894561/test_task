import React from 'react';
import axios from 'axios';


let ids = [];


export default class Contacts extends React.Component {


    state = {
        contacts: [],
    }
    
  componentDidMount() {
    axios.get(`https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`)
      .then(res => {
        const contacts = res.data;
        this.setState({ contacts });
      })
    }
    handleClick(e) {
        if (!ids.includes(e.target.id)) {
            ids.push(e.target.id);              
        } else {
            ids.splice(ids.indexOf(e.target.id), 1);
        }
        console.log("ids: ",ids);
     }
    
    

    render() {

    return (

            <React.Fragment>
                { this.state.contacts.sort(((a, b) => a.last_name.localeCompare(b.last_name))).map(
                contact => <div className="contact" key={contact.id} >
                        <img src={contact.avatar} key={contact.id} style={{width:"50px",height:"50px"}} alt=""></img> {contact.first_name} {contact.last_name}
                        
                        <input type="checkbox" id={contact.id} onClick={this.handleClick} style={{cursor: 'pointer'}} />

                    </div>)}
                   </React.Fragment> 

      
    )
  }
}